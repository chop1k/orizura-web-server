<?php

use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use function Symfony\Component\DependencyInjection\Loader\Configurator\param;

/**
 * The function defines services via {@link ContainerConfigurator}.
 *
 * @param ContainerConfigurator $configurator
 */
return static function (ContainerConfigurator $configurator): void
{
    $services = $configurator->services();

    $services
        ->set('http.client', HttpClientInterface::class)
            ->factory([HttpClient::class, 'create'])
    ;

    $services
        ->set('validation.manager', ValidatorInterface::class)
            ->factory([Validation::class, 'createValidator'])
    ;
};