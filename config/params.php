<?php

use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;

/**
 * The function defines parameters via {@link ContainerConfigurator}.
 *
 * @param ContainerConfigurator $container
 */
return static function (ContainerConfigurator $container): void
{
    $root = dirname(__DIR__);

    $container->parameters()
        ->set('app.path', $root)
        ->set('app.path.entities', $root.'/Entity')
        ->set('db.config', [
            'user' => $_ENV['DB_USER'],
            'password' => $_ENV['DB_PASSWORD'],
            'dbname' => $_ENV['DB_NAME'],
            'host' => $_ENV['DB_HOST'],
            'port' => $_ENV['DB_PORT'],
            'driver' => $_ENV['DB_DRIVER']
        ])
        ->set('api.token', $_ENV['API_TOKEN'])
        ->set('api.url', $_ENV['API_URL'])
        ->set('app.domain', $_ENV['APP_DOMAIN'])
    ;
};