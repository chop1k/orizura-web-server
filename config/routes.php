<?php

use Orizura\Web\Validator\UserValidator;
use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;

/**
 * The function defines routes via {@link RoutingConfigurator}.
 *
 * @param RoutingConfigurator $routes
 */
return static function (RoutingConfigurator $routes): void
{
    $onlyPost = ['POST'];

    $routes
        ->add('user.login', '/u/login')
            ->controller(['user.controller', 'login'])
            ->methods($onlyPost)
            ->defaults([
                'validator' => UserValidator::class
            ])
    ;

    $routes
        ->add('user.register', '/u/register')
            ->controller(['user.controller', 'register'])
            ->methods($onlyPost)
            ->defaults([
                'validator' => UserValidator::class
            ])
    ;
};
