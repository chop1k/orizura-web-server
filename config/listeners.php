<?php

use Orizura\Web\Listener\ExceptionListener;
use Orizura\Web\Listener\RequestListener;
use Orizura\Web\Listener\ResponseListener;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\HttpKernel\KernelEvents;
use function Symfony\Component\DependencyInjection\Loader\Configurator\service;

/**
 * The function defines listeners via {@link ContainerConfigurator}.
 *
 * @param ContainerConfigurator $container
 */
return static function (ContainerConfigurator $container): void
{
    $services = $container->services();

    $services
        ->set('kernel.request.listener', RequestListener::class)
            ->args([
                service('app.config.locator'),
                service('validation.manager')
            ])
            ->tag('app.listener', [KernelEvents::REQUEST, 'onRequest'])
    ;

    $services
        ->set('kernel.exception.listener', ExceptionListener::class)
            ->tag('app.listener', [KernelEvents::EXCEPTION, 'onException'])
    ;

    $services
        ->set('kernel.response.listener', ResponseListener::class)
            ->tag('app.listener', [KernelEvents::RESPONSE, 'onResponse'])
        ;
};