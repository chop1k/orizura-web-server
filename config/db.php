<?php

use Doctrine\ORM\Configuration;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;
use Doctrine\Persistence\Mapping\Driver\StaticPHPDriver;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use function Symfony\Component\DependencyInjection\Loader\Configurator\inline_service;
use function Symfony\Component\DependencyInjection\Loader\Configurator\param;
use function Symfony\Component\DependencyInjection\Loader\Configurator\service;

/**
 * The function defines database services via {@link ContainerConfigurator.
 *
 * @param ContainerConfigurator $container
 */
return static function (ContainerConfigurator $container): void
{
    $services = $container->services();

    $driver = inline_service(StaticPHPDriver::class)
        ->args([
            param('app.path.entities')
        ])
    ;

    $services
        ->set('db.orm.config', Configuration::class)
            ->call('setMetadataDriverImpl', [$driver])
    ;

    $services
        ->get('db.orm.config')
            ->factory([Setup::class, 'createConfiguration'])
    ;

    $services
        ->set('db.entity.manager', EntityManager::class)
            ->factory([EntityManager::class, 'create'])
                ->args([
                    param('db.config'),
                    service('db.orm.config')
                ])
    ;
};