<?php

use Orizura\Web\Controller\UserController;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use function Symfony\Component\DependencyInjection\Loader\Configurator\param;
use function Symfony\Component\DependencyInjection\Loader\Configurator\service;

/**
 * The function defines controllers via {@link ContainerConfigurator}.
 *
 * @param ContainerConfigurator $container
 */
return static function(ContainerConfigurator $container): void
{
    $services = $container->services();

    $services
        ->set('user.controller', UserController::class)
            ->args([
                service('http.client'),
                param('api.url'),
                param('api.token'),
                param('app.domain')
            ])
    ;
};