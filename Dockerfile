FROM php:7.4-fpm

COPY composer.json composer.lock /var/www/web.orizura.org/

WORKDIR /var/www/web.orizura.org