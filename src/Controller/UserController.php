<?php

namespace Orizura\Web\Controller;

use Carbon\Carbon;
use Orizura\Web\Exception\ApiException;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

/**
 * Class UserController handles user-related requests.
 *
 * @package Orizura\Web\Controller
 */
class UserController
{
    /**
     * Http client to sending requests to the api.
     *
     * @var HttpClientInterface $client
     */
    protected HttpClientInterface $client;

    /**
     * Api url.
     *
     * @var string $url
     */
    protected string $url;

    /**
     * Api token.
     *
     * @var string $token
     */
    protected string $token;

    /**
     * Domain to set cookies.
     *
     * @var string $domain
     */
    protected string $domain;

    /**
     * UserController constructor.
     *
     * @param HttpClientInterface $client
     *
     * @param string $url
     *
     * @param string $token
     *
     * @param string $domain
     */
    public function __construct(HttpClientInterface $client, string $url, string $token, string $domain)
    {
        $this->client = $client;
        $this->url = $url;
        $this->token = $token;
        $this->domain = $domain;
    }

    /**
     * Wrapper for apis login action which sends request to api and returns result with cookie.
     *
     * @param Request $request
     *
     * @return Response
     *
     * @throws ApiException
     *
     * @throws ClientExceptionInterface
     *
     * @throws DecodingExceptionInterface
     *
     * @throws RedirectionExceptionInterface
     *
     * @throws ServerExceptionInterface
     *
     * @throws TransportExceptionInterface
     */
    public function login(Request $request): Response
    {
        return $this->response($this->createToken(
            $request->request->get('name'),
            hash('sha256', $request->request->get('password'))
        ));
    }

    /**
     * Sends request to create user api action and then authenticates user and returns response with cookie.
     *
     * @param Request $request
     *
     * @return Response
     *
     * @throws ApiException
     *
     * @throws ClientExceptionInterface
     *
     * @throws DecodingExceptionInterface
     *
     * @throws RedirectionExceptionInterface
     *
     * @throws ServerExceptionInterface
     *
     * @throws TransportExceptionInterface
     */
    public function register(Request $request): Response
    {
        $name = $request->request->get('name');
        $password = hash('sha256', $request->request->get('password'));

        $this->createUser($name, $request->request->get('email'), $password);

        return $this->response($this->createToken($name, $password));
    }

    /**
     * Shortcut for sending request to the authentication api action.
     *
     * @param string $name
     *
     * @param string $password
     *
     * @return string
     *
     * @throws ApiException
     *
     * @throws ClientExceptionInterface
     *
     * @throws DecodingExceptionInterface
     *
     * @throws RedirectionExceptionInterface
     *
     * @throws ServerExceptionInterface
     *
     * @throws TransportExceptionInterface
     */
    private function createToken(string $name, string $password): string
    {
        $response = $this->client->request('POST', sprintf('%s/t/create', $this->url), [
            'json' => [
                'name' => $name,
                'password' => $password,
            ],
            'headers' => [
                'token' => $this->token
            ]
        ]);

        $this->checkResponse($response);


        return $response->toArray(false)['token'];
    }

    /**
     * Shortcut for sending request to the create user api action.
     *
     * @param string $name
     *
     * @param string $email
     *
     * @param string $password
     *
     * @return int
     *
     * @throws ApiException
     *
     * @throws ClientExceptionInterface
     *
     * @throws DecodingExceptionInterface
     *
     * @throws RedirectionExceptionInterface
     *
     * @throws ServerExceptionInterface
     *
     * @throws TransportExceptionInterface
     */
    private function createUser(string $name, string $email, string $password): int
    {
        $response = $this->client->request('POST', sprintf('%s/u/create', $this->url), [
            'json' => [
                'name' => $name,
                'email' => $email,
                'password' => $password
            ],
            'headers' => [
                'token' => $this->token
            ]
        ]);

        $this->checkResponse($response);

        return $response->toArray(false)['id'];
    }

    /**
     * Checks response status and throws exception.
     *
     * @param ResponseInterface $response
     *
     * @throws ApiException
     *
     * @throws ClientExceptionInterface
     *
     * @throws DecodingExceptionInterface
     *
     * @throws RedirectionExceptionInterface
     *
     * @throws ServerExceptionInterface
     *
     * @throws TransportExceptionInterface
     */
    private function checkResponse(ResponseInterface $response): void
    {
        $status = $response->getStatusCode();

        if ($status !== 200)
        {
            throw ApiException::apiCallException($response->toArray(false), $status);
        }
    }

    /**
     * Returns response with token and cookie.
     *
     * @param string $token
     *
     * @return JsonResponse
     */
    private function response(string $token): JsonResponse
    {
        $json = new JsonResponse([
            'token' => $token
        ]);

        $cookie = Cookie::create('token')
            ->withExpires(Carbon::now()->addYear()->unix())
            ->withHttpOnly()
            ->withValue($token)
            ->withPath('/')
            ->withSameSite('None')
        ;

        $json->headers->setCookie($cookie);

        return $json;
    }
}