<?php

namespace Orizura\Web\Exception;

/**
 * Class ApiException represents api exception with response.
 *
 * @package Orizura\Web\Exception
 */
class ApiException extends Exception
{
    /**
     * Api call response.
     *
     * @var mixed $response
     */
    protected $response;

    /**
     * Sets api call response.
     *
     * @param mixed $response
     */
    public function setResponse($response): void
    {
        $this->response = $response;
    }

    /**
     * @inheritdoc
     */
    public function jsonSerialize(): array
    {
        $json = parent::jsonSerialize();

        $json['previous'] = $this->response;

        return $json;
    }

    /**
     * Returns exception with api response and standard message.
     *
     * @param $response
     *
     * @param int $status
     *
     * @return ApiException
     */
    public static function apiCallException($response, int $status): ApiException
    {
        $exception = new ApiException('Api returned wrong response. ');

        $exception->setResponse($response);
        $exception->setStatus($status);

        return $exception;
    }
}