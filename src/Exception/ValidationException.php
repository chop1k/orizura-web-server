<?php

namespace Orizura\Web\Exception;

use Symfony\Component\Validator\ConstraintViolationList;

/**
 * Class ValidationException represents validation exception with violations.
 *
 * @package Orizura\Exception
 */
class ValidationException extends Exception
{
    /**
     * List of violations.
     *
     * @var ConstraintViolationList $list
     */
    protected ConstraintViolationList $list;

    /**
     * ValidationException constructor.
     *
     * @param ConstraintViolationList $list
     */
    public function __construct(ConstraintViolationList $list)
    {
        $this->list = $list;

        parent::__construct('Validation failed. ');

        $this->status = 400;
    }

    /**
     * @inheritdoc
     *
     * @throws \Exception
     */
    public function jsonSerialize(): array
    {
        $json = parent::jsonSerialize();

        $violations = [];

        foreach ($this->list->getIterator() as $violation)
        {
            $violations[] = [
                'code' => $violation->getCode(),
                'message' => $violation->getMessage(),
                'invalidValue' => $violation->getInvalidValue(),
            ];
        }

        $json['data']['violations'] = $violations;

        return $json;
    }
}