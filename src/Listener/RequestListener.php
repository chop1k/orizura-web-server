<?php

namespace Orizura\Web\Listener;

use Orizura\Web\Exception\ValidationException;
use Orizura\Web\Validator\ControllerValidator;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpFoundation\Exception\JsonException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\Loader\PhpFileLoader as RouterPhpLoader;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class RequestListener listens the {@link KernelEvents::REQUEST} event.
 *
 * @package Orizura\Listener
 */
class RequestListener
{
    /**
     * FileLocator locates the config directory.
     *
     * @var FileLocator $locator
     */
    protected FileLocator $locator;

    /**
     * Validation service needed for validation.
     *
     * @var ValidatorInterface $validator
     */
    protected ValidatorInterface $validator;

    /**
     * RequestListener constructor.
     *
     * @param FileLocator $locator
     *
     * @param ValidatorInterface $validator
     *
     */
    public function __construct(FileLocator $locator, ValidatorInterface $validator)
    {
        $this->locator = $locator;
        $this->validator = $validator;
    }

    /**
     * Executes when {@link KernelEvents::REQUEST} event dispatched.
     *
     * @param RequestEvent $event
     *
     * @throws ValidationException
     */
    public function onRequest(RequestEvent $event): void
    {
        $request = $event->getRequest();

        if ($request->getMethod() === 'OPTIONS')
        {
            $event->setResponse(new Response('OK', 200));

            return;
        }

        $this->handleRoutes($request);
        $this->handleJson($request);
        $this->handleValidation($request);
    }

    /**
     * Gets routes from the configuration file and matches it with the request url.
     *
     * @param Request $request
     */
    protected function handleRoutes(Request $request): void
    {
        $context = (new RequestContext())->fromRequest($request);

        $routes = (new RouterPhpLoader($this->locator))->load('routes.php');

        $request->attributes->replace(
            (new UrlMatcher($routes, $context))->matchRequest($request)
        );
    }

    /**
     * If the request is a json request, it parses the request body and passes the result to the request bag.
     *
     * @param Request $request
     */
    protected function handleJson(Request $request): void
    {
        if ($request->headers->get('content-type', false) === 'application/json' && $request->getMethod() === 'POST')
        {
            $json = json_decode($request->getContent(), true);

            if (is_null($json))
            {
                throw new JsonException('Cannot decode given data.');
            }

            $request->request->replace($json);
        }
    }

    /**
     * If the controller has a validator, it processes the request data through the validator.
     *
     * @param Request $request
     *
     * @throws ValidationException
     */
    protected function handleValidation(Request $request): void
    {
        /**
         * @var ControllerValidator $validator
         */
        $validator = $request->attributes->get('validator', false);

        if ($validator === false || !class_exists($validator))
        {
            return;
        }

        if (!in_array(ControllerValidator::class, class_implements($validator)))
        {
            return;
        }

        $methods = $validator::getAliases();
        $method = $request->attributes->get('_controller')[1];

        if (!isset($methods[$method]))
        {
            return;
        }

        $method = $methods[$method];

        /**
         * @var ConstraintViolationList $violations
         */
        $violations = $this->validator->validate($request->request->all(), $validator::$method());

        if ($violations->count() > 0)
        {
            throw new ValidationException($violations);
        }
    }
}