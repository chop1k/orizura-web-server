<?php

namespace Orizura\Web\Listener;

use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class ResponseListener listens the {@link KernelEvents::RESPONSE} event.
 *
 * @package Orizura\Listener
 */
class ResponseListener
{
    /**
     * Sets cors headers to the response.
     *
     * @param ResponseEvent $event
     */
    public function onResponse(ResponseEvent $event): void
    {
        $response = $event->getResponse();

        $request = $event->getRequest();

        $response->headers->set('Access-Control-Allow-Origin', $request->headers->get('Origin'));
        $response->headers->set('Access-Control-Allow-Credentials', 'true');
        $response->headers->set('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
        $response->headers->set('Access-Control-Allow-Headers', 'DNT, X-User-Token, Keep-Alive, User-Agent, X-Requested-With, If-Modified-Since, Cache-Control, Content-Type');
        $response->headers->set('Access-Control-Max-Age', 1728000);
        
        $event->setResponse($response);
    }
}