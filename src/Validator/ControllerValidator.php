<?php

namespace Orizura\Web\Validator;

use Symfony\Component\Validator\Constraints\Collection;

/**
 * Interface ControllerValidator defines the required functionality of the controller validator.
 *
 * @package Orizura\Validator
 */
interface ControllerValidator
{
    /**
     * Returns an array where the keys represent the controller method, and the values represent the method that should return {@link Collection}.
     *
     * @return array
     */
    public static function getAliases(): array;
}