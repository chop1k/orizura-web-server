<?php

namespace Orizura\Web;

use Exception;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\EventDispatcher\EventDispatcher;

/**
 * Class ContainerEventDispatcher extends the functionality of the {@link EventDispatcher} allowing it to use container dependencies.
 *
 * @package Orizura
 */
class ContainerEventDispatcher extends EventDispatcher
{
    /**
     * Container with dependencies.
     *
     * @var Container $container
     */
    protected Container $container;

    /**
     * ContainerEventDispatcher constructor.
     *
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;

        parent::__construct();
    }

    /**
     * Overrides callListeners method of parent adding the ability to use container dependencies.
     *
     * @param iterable $listeners
     *
     * @param string $eventName
     *
     * @param object $event
     *
     * @throws Exception
     */
    protected function callListeners(iterable $listeners, string $eventName, object $event)
    {
        foreach ($listeners as $listener)
        {
            $instance = $this->container->get($listener[0]);

            $method = $listener[1];

            $instance->$method($event, $eventName, $this);
        }
    }
}