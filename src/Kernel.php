<?php


namespace Orizura\Web;

use Exception;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\PhpFileLoader;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver;
use Symfony\Component\HttpKernel\Controller\ContainerControllerResolver;
use Symfony\Component\HttpKernel\HttpKernel;

/**
 * Class Kernel
 *
 * @package Orizura
 */
class Kernel extends HttpKernel
{
    /**
     * Loads configuration via {@link PhpFileLoader}
     *
     * @param ContainerBuilder $builder
     *
     * @throws Exception
     */
    protected function loadConfig(ContainerBuilder $builder): void
    {
        /**
         * @var FileLocator $locator
         */
        $locator = $builder->get('app.config.locator');

        $loader = new PhpFileLoader($builder, $locator);

        $loader->load('params.php');
        $loader->load('db.php');
        $loader->load('services.php');
        $loader->load('controllers.php');
        $loader->load('listeners.php');
    }

    /**
     * Returns the dispatcher with listeners.
     *
     * @param ContainerBuilder $container
     *
     * @return EventDispatcherInterface
     */
    protected function createDispatcher(ContainerBuilder $container): EventDispatcherInterface
    {
        $dispatcher = new ContainerEventDispatcher($container);

        $listeners = $container->findTaggedServiceIds('app.listener');

        foreach ($listeners as $name => $listener)
        {
            $listener = $listener[0];

            $dispatcher->addListener($listener[0], [$name, $listener[1]]);
        }

        return $dispatcher;
    }

    /**
     * Registers additional dependencies.
     *
     * @param ContainerBuilder $container
     */
    protected function registerDependencies(ContainerBuilder $container): void
    {
        $container
            ->register('app.config.locator', FileLocator::class)
            ->setArguments([
                dirname(__DIR__).'/config'
            ])
        ;
    }

    /**
     * Kernel constructor.
     * @throws Exception
     */
    public function __construct()
    {
        $container = new ContainerBuilder();

        $this->registerDependencies($container);

        $this->loadConfig($container);

        parent::__construct(
            $this->createDispatcher($container),
            new ContainerControllerResolver($container),
            new RequestStack(),
            new ArgumentResolver()
        );
    }
}